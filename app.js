var express         = require("express"),
    app             = express(),
    bodyParser      = require("body-parser"),
    methodOverride  = require("method-override"),
    mongoose        = require('mongoose');

// Connection to DB
mongoose.connect('mongodb://localhost/tvshows', function(err, res) {
  if(err) throw err;
  console.log('Connected to Database');
});

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

//Use This to block Message "Cross-Origin Request Blocked" on Browsers
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

// Import Models and controllers
var models     = [require('./models/tvshow')(app, mongoose),
                  require('./models/person')(app, mongoose)];
var TVShowCtrl = require('./controllers/tvshows');
var PersonCtrl = require('./controllers/persons');

// Example Route
var router = express.Router();
router.get('/', function(req, res) {
  res.send("Hello world!");
});
app.use(router);

// API routes
var tvshow = express.Router();

tvshow.route('/tvshows')
  .get(TVShowCtrl.findAllTVShows)
  .post(TVShowCtrl.addTVShow);

tvshow.route('/tvshows/:id')
  .get(TVShowCtrl.findById)
  .put(TVShowCtrl.updateTVShow)
  .delete(TVShowCtrl.deleteTVShow);

tvshow.route('/tvshows/:id/deleteAllCast')
  .put(TVShowCtrl.deleteTVShowCast);

tvshow.route('/tvshows/:id/deleteActorCast')
  .put(TVShowCtrl.deleteActorTVShowCast);

tvshow.route('/persons')
  .get(PersonCtrl.findAllPersons)
  .post(PersonCtrl.addPerson);

tvshow.route('/persons/:id')
  .get(PersonCtrl.findPersonById)
  .put(PersonCtrl.updatePerson)
  .delete(PersonCtrl.deletePerson);

app.use('/api', tvshow);

// Start server
app.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});