//File: controllers/persons.js
var mongoose = require('mongoose');
var Person  = mongoose.model('Person');

//GET - Return all Persons in the DB
exports.findAllPersons = function(req, res) {
	Person.find(function(err, persons) {
    	if(err) res.status(500).send(err.message);
		console.log('GET /persons')
		res.status(200).jsonp(persons);
	});
};

//GET - Return a TVShow with specified ID
exports.findPersonById = function(req, res) {
	Person.findById(req.params.id, function(err, person) {
    	if(err) return res.status(500).send(err.message);
		console.log('GET /person/' + req.params.id);
		res.status(200).jsonp(person);
	});
};

//POST - Insert a new Person in the DB
exports.addPerson = function(req, res) {
	console.log('POST');
	console.log(req.body);

	var person = new Person({
			name: 		req.body.name,
			lastname: 	req.body.lastname,
			country:  	req.body.country,
			gender: 	req.body.gender,
			summary: 	req.body.summary
		});

	person.save(function(err, person) {
		if(err) return res.status(500).send(err.message);
    	res.status(200).jsonp(person);
	});
};

//PUT - Update a register already exists
exports.updatePerson = function(req, res) {
	TVShow.findById(req.params.id, function(err, person) {
		person.name 	= req.body.name || person.name;
		person.lastname = req.body.lastname || person.lastname;
		person.country 	= req.body.country || person.country;
		person.gender 	= req.body.gender || person.gender;
		person.summary 	= req.body.summary || person.summary;

		person.save(function(err) {
			if(err) return res.status(500).send(err.message);
      		res.status(200).jsonp(person);
		});
	});
};

//DELETE - Delete a TVShow with specified ID
exports.deletePerson = function(req, res) {
	var personID = req.params.id;
	/*TVShow.findById(req.params.id, function(err, tvshow) {
		if(err) return res.send(500, err.message);
		tvshow.remove(function(err) {
			if(err) return res.send(500, err.message);
      		console.log('DELETE /tvshows/' + showID);
      		res.status(200).jsonp('');
		})
	});*/

	Person.findOneAndRemove({_id:req.params.id}, function(err, person) {
			if(err) return res.status(500).send(err.message);
      		console.log('FIND ONE AND DELETE /person/' + personID);
      		res.status(200).jsonp(null);
	});

	/*TVShow.findOneAndRemove({id:showID}).remove(function(err) {
			if(err) return res.send(500, err.message);
      		console.log('DELETE /tvshows/' + showID);
      		res.status(200);
		});*/

};