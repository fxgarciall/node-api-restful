//File: controllers/tvshows.js
var mongoose = require('mongoose');
var TVShow  = mongoose.model('TVShow');

//GET - Return all tvshows in the DB
exports.findAllTVShows = function(req, res) {
	TVShow.find(function(err, tvshows) {
    	if(err) res.status(500).send(err.message);
		console.log('GET /tvshows')
		res.status(200).jsonp(tvshows);
	}).populate('cast.person');
};

//GET - Return a TVShow with specified ID
exports.findById = function(req, res) {
	TVShow.findById(req.params.id, function(err, tvshow) {
    	if(err) return res.status(500).send(err.message);
		console.log('GET /tvshow/' + req.params.id);
		res.status(200).jsonp(tvshow);
	}).populate('cast.person');
};

//POST - Insert a new TVShow in the DB
exports.addTVShow = function(req, res) {
	console.log('POST');
	console.log(req.body);

	var tvshow = new TVShow({
			title:    req.body.title,
			year: 	  req.body.year,
			country:  req.body.country,
			poster:   req.body.poster,
			seasons:  req.body.seasons,
			genre:    req.body.genre,
			summary:  req.body.summary,
			cast: 	  req.body.cast
		});

	tvshow.save(function(err, tvshow) {
		if(err) return res.status(500).send(err.message);
    	res.status(200).jsonp(tvshow);
	});
};

//PUT - Update a register already exists
exports.updateTVShow = function(req, res) {
	TVShow.findById(req.params.id, function(err, tvshow) {
		if(err) return res.status(500).send(err.message);
		if(tvshow===null) return res.status(500).send("TVShow Does not exists");
		
		tvshow.title   = req.body.title || tvshow.title;
		tvshow.year    = req.body.year || tvshow.year;
		tvshow.country = req.body.country || tvshow.country;
		tvshow.poster  = req.body.poster || tvshow.poster;
		tvshow.seasons = req.body.seasons || tvshow.seasons;
		tvshow.genre   = req.body.genre || tvshow.genre;
		tvshow.summary = req.body.summary || tvshow.summary;
		
		if(req.body.cast){
			tvshow.cast	   = arrayUnique(tvshow.cast.concat(req.body.cast));
		}else{
			tvshow.cast	   = tvshow.cast;	
		}

		tvshow.save(function(err) {
			if(err) return res.status(500).send(err.message);
      		res.status(200).jsonp(tvshow);
		});
	});
};

//DELETE - Delete a TVShow with specified ID
exports.deleteTVShow = function(req, res) {
	var showID = req.params.id;
	/*TVShow.findById(req.params.id, function(err, tvshow) {
		if(err) return res.send(500, err.message);
		tvshow.remove(function(err) {
			if(err) return res.send(500, err.message);
      		console.log('DELETE /tvshows/' + showID);
      		res.status(200).jsonp('');
		})
	});*/

	TVShow.findOneAndRemove({_id:req.params.id}, function(err, tvshow) {
			if(err) return res.status(500).send(err.message);
      		console.log('FIND ONE AND DELETE /tvshows/' + showID);
      		res.status(200).jsonp(null);
	});

	/*TVShow.findOneAndRemove({id:showID}).remove(function(err) {
			if(err) return res.send(500, err.message);
      		console.log('DELETE /tvshows/' + showID);
      		res.status(200);
		});*/

};

//PUT - Delete All TV Show Cast with PUT
exports.deleteTVShowCast = function(req, res) {
	TVShow.findById(req.params.id, function(err, tvshow) {
		if(err) return res.status(500).send(err.message);
		if(tvshow===null) return res.status(500).send("TVShow Does not exists");
		
		tvshow.title   = req.body.title || tvshow.title;
		tvshow.year    = req.body.year || tvshow.year;
		tvshow.country = req.body.country || tvshow.country;
		tvshow.poster  = req.body.poster || tvshow.poster;
		tvshow.seasons = req.body.seasons || tvshow.seasons;
		tvshow.genre   = req.body.genre || tvshow.genre;
		tvshow.summary = req.body.summary || tvshow.summary;
		tvshow.cast = [];

		tvshow.save(function(err) {
			if(err) return res.status(500).send(err.message);
      		res.status(200).jsonp(tvshow);
		});
	});
};

//PUT - Delete Single Actor TV Show Cast
exports.deleteActorTVShowCast = function(req, res) {
	TVShow.findById(req.params.id, function(err, tvshow) {
		if(err) return res.status(500).send(err.message);
		if(tvshow===null) return res.status(500).send("TVShow Does not exists");
		if(!req.body.id) return res.status(500).send("Must populate Actor ID");

		tvshow.title   = tvshow.title;
		tvshow.year    = tvshow.year;
		tvshow.country = tvshow.country;
		tvshow.poster  = tvshow.poster;
		tvshow.seasons = tvshow.seasons;
		tvshow.genre   = tvshow.genre;
		tvshow.summary = tvshow.summary;
		tvshow.cast = deleteFromArray(tvshow.cast,req.body.id);

		tvshow.save(function(err) {
			if(err) return res.status(500).send(err.message);
      		res.status(200).jsonp(tvshow);
		});
	});
};

function arrayUnique(array) {
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i].id === a[j].id)
                a.splice(j--, 1);
        }
    }
    return a;
}

function deleteFromArray(array,id){
	a = array.concat();
	for(var i=0; i<a.length; ++i) {
		if(a[i].person == id){
			console.log(a[i].person);
			a.splice(i, 1);
		}
	}
	return a;
}