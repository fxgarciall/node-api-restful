exports = module.exports = function(app, mongoose) {

	var personSchema = new mongoose.Schema({
		name: 		{ type: String },
		lastname:	{ type: String },
		country: 	{ type: String },
		gender:		{
			type: String,
			enum: ['M', 'F']
		},
		summary: 	{ type: String }
	});

	mongoose.model('Person', personSchema);

};