exports = module.exports = function(app, mongoose) {

	var tvshowSchema = new mongoose.Schema({
		title: 		{ type: String },
		year: 		{ type: Number },
		country: 	{ type: String },
		poster:  	{ type: String },
		seasons: 	{ type: Number },
		genre: 		{
			type: String,
			enum: ['Drama', 'Fantasy', 'Sci-Fi', 'Thriller', 'Comedy']
		},
		summary: 	{ type: String },
		cast: [{
			person: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Person'
			}
		}]
	});

	mongoose.model('TVShow', tvshowSchema);

};